# Demo Project - Complete CI/CD Pipeline with EKS  and AWS ECR 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Create private AWS ECR Docker repository

* Adjust Jenkinsfile to build and push Docker Image to AWS ECR

* Integrate deploying to K8s cluster in the CI/CD pipeline from AWS ECR private registry

* Complete CI/CD project has the following configuration:

a. CI step: Increment version

b. CI step: Build artifact for Java Maven application

c. CI step: Build and push Docker image to AWS ECR

d. CD step: Deploy new application version to EKS cluster 

e. CD step: Commit the version update

## Technologies Used 

* Kubernetes 

* Jenkins 

* AWS EKS 

* AWS ECR 

* Java 

* Maven 

* Linux 

* Docker 

* Git 

## Steps 

Step 1: Create private ECR 

[Creating private ECR](/images/01_creating_private_ecr.png)

[Created private ECR](/images/02_created_private_ecr.png)

Step 2: Create ECR credential on Jenkins globally 

[ECR Jenkins cred](/images/03_create_ecr_credential_globally_on_jenkins.png)

Step 3: Create secret for kubernetes, this is because you will be pulling the image from ecr

    kubectl create secret docker-registry aws-registry-key --docker-server=522116691231.dkr.ecr.eu-west-3.amazonaws.com --docker-username=AWS --docker-password=xxx 

[Creating secret in k8 for ecr](/images/04_create_secret_for_kubernetes_because_you_will_be_pulling_the_image.png)

[Secre created](/images/05_secret_created.png)

Step 4: Change image pull secret in deployment manifest file to new secret 

[Changing image pull secret](/images/06_change_image_pull_secret_in_deployment_file_to_new_secret.png)

Step 5: Create build stage with tags dynamically

[Dynamic tagging](/images/07_create_build_stage_with_tag_dynamically.png)

Step 6: Set docker repo as q global environmental variables in Jenkinsfile for all stages 

[Docker repo env var](/images/08_set_global_environment_for_all_stages.png)

Step 7: Add correct image name in deployment manifest file for pod configuration

[Image name in pod config](/images/09_add_correct_image_name_in_deployment_file_for_pod_configuration.png)

Step 8: Insert docker repo server as a global environemtnal variable for all stages

[docker repo server env var](/images/10_insert_docker_repo_server_as_a_global_environmental_variable_for_all_stages.png)

Step 9: Pass the server environmetnal variable as a third parameter for docker login in build image stage

[docker login](/images/11_pass_the_server_environmental_variable_as_a_third_parameter_for_docker_login_in_build_image_stage.png)

Step 10: Build the Pipeline 

[Completed Pipeline](/images/12_completed_pipeline.png)

[Console output](/images/13_console_output.png)

Step 11: Check if application was successfully deployed on k8 cluster

[Running pods](/images/14_running_pod.png)  

## Installation

    apt-get install gettext-base

## Usage 

    kubectl create secret docker-registry my-registry-key \
    --docker-server=docker.io \
    --docker-username=omacodes98 \
    --docker-password=xxxx


## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/complete-ci-cd-pipeline-with-eks-and-aws-ecr.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/complete-ci-cd-pipeline-with-eks-and-aws-ecr

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.

## License